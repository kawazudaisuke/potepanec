module ApplicationHelper
  def html_title(title = "")
    if title.empty?
      "BIGBAG Store"
    else
      "#{title} - BIGBAG Store"
    end
  end
end
