module Potepan
  class ProductsController < ApplicationController
    def show
      @product = Spree::Product.find(params[:id])
      @taxon = @product.taxons.find_by(taxonomy_id: params[:taxonomy_id])
    end
  end
end
