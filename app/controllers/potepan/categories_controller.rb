module Potepan
  class CategoriesController < ApplicationController
    def show
      @taxon = Spree::Taxon.find(params[:id])
      @product = @taxon.all_products.includes(master: [:default_price, :images])
      @taxonomies = Spree::Taxonomy.all.includes(:taxons)
    end
  end
end
