require 'rails_helper'
require 'capybara/rspec'

RSpec.describe "Products", type: :helper do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "html_title" do

    before do
      visit potepan_product_path(id: product.id, taxonomy_id: taxon.taxonomy_id)
    end

    it "shows right title" do
      expect(page).to have_title(product.name)
    end
  end
end
