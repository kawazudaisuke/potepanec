require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_product_path(id: product.id, taxonomy_id: taxon.taxonomy_id)
  end

  scenario "show the data from the database" do
    expect(page).to have_text(product.name)
    expect(page).to have_text(product.price)
  end

  scenario "visit the top_page" do
    within ".nav" do
      click_link "Home"
      expect(page).to have_current_path potepan_index_path
    end
  end

  scenario "visit the category_page" do
    click_link "一覧ページへ戻る"
    expect(page).to have_current_path potepan_category_path(taxon.id)
  end
end
