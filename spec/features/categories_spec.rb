require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, name: "hoge", price: 100) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "click on the taxonomies, and show its children" do
    click_on taxonomy.name
    within '.side-nav' do
      expect(page).to have_content taxon.name
    end

    within "#productBox" do
      expect(page).to have_content product.name
      expect(page).to have_content product.price
      expect(page).not_to have_content unrelated_product.name
      expect(page).not_to have_content unrelated_product.price
    end
  end
end
