require 'rails_helper'

RSpec.describe "Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  describe "Get #show" do
    before do
      get potepan_category_path(taxon.id)
    end

    it 'move on to categories#show' do
      expect(response).to be_successful
    end

    it 'response include product' do
      expect(response.body).to include(product.name)
    end
    it 'response include taxon' do
      expect(response.body).to include(taxon.name)
    end
    it 'response include taxonomy' do
      expect(response.body).to include(taxonomy.name)
    end

  end
end
