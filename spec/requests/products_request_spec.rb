require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "Get #show" do
    before do
      get potepan_product_path(id: product.id, taxonomy_id: taxon.taxonomy_id)
    end

    it 'move on to products#show' do
      expect(response).to be_successful
    end

    it 'response include product' do
      expect(response.body).to include(product.name)
    end
  end
end
